unit uIdTcpClientTools;

interface

uses
  Classes, IdGlobal, IdIOHandlerSocket, SysUtils, IdTCPClient;

type
  TIdTcpClientTools = class(TObject)
  public
    class function recvBuffer(pvSocket: TIdIOHandlerSocket; buf: Pointer; len:
        Cardinal): Integer;
    class function sendBuffer(pvSocket: TIdIOHandlerSocket; buf: Pointer; len:
        Cardinal): Integer;
    class function sendStream(pvSocket: TIdIOHandlerSocket; pvStream: TStream):
        Integer;

    /// <summary>
    ///   安静关闭
    /// </summary>
    class procedure slienceClose(pvSocket: TIdTcpClient);

    /// <summary>
    ///   判断连接是否已经连接
    /// </summary>
    class function isConnected(pvSocket: TIdTcpClient): Boolean;
  end;

implementation


const
  BUF_BLOCK_SIZE = 1024;

class function TIdTcpClientTools.isConnected(pvSocket: TIdTcpClient): Boolean;
begin
  try
    Result := pvSocket.Connected;
  except
    Result := false;
    slienceClose(pvSocket);
  end;
end;

class function TIdTcpClientTools.recvBuffer(pvSocket: TIdIOHandlerSocket; buf:
    Pointer; len: Cardinal): Integer;
var
  lvBuf: TIdBytes;
begin
  pvSocket.ReadBytes(lvBuf, len);
  Result := IndyLength(lvBuf);
  Move(lvBuf[0], buf^, Result);
  SetLength(lvBuf, 0);
end;

class function TIdTcpClientTools.sendBuffer(pvSocket: TIdIOHandlerSocket; buf:
    Pointer; len: Cardinal): Integer;
var
  lvBytes:TIdBytes;
begin
  SetLength(lvBytes, len);
  Move(buf^, lvBytes[0], len);
  pvSocket.Write(lvBytes, len);
  SetLength(lvBytes, 0);
  Result := len;
end;

class function TIdTcpClientTools.sendStream(pvSocket: TIdIOHandlerSocket;
    pvStream: TStream): Integer;
var
  lvBufBytes:array[0..BUF_BLOCK_SIZE-1] of byte;
  l, j, lvTotal:Integer;
begin
  Result := 0;
  if pvStream = nil then Exit;
  if pvStream.Size = 0 then Exit;

  lvTotal :=0;
  
  pvStream.Position := 0;
  repeat
    //FillMemory(@lvBufBytes[0], SizeOf(lvBufBytes), 0);
    l := pvStream.Read(lvBufBytes[0], SizeOf(lvBufBytes));
    if (l > 0) and pvSocket.Connected then
    begin
      j:=sendBuffer(pvSocket, @lvBufBytes[0], l);
      if j <> l then
      begin
        raise Exception.CreateFmt('发送Buffer错误指定发送%d,实际发送:%d', [j, l]);
      end else
      begin
        lvTotal := lvTotal + j;
      end;
    end else Break;
  until (l = 0);
  Result := lvTotal;
end;

class procedure TIdTcpClientTools.slienceClose(pvSocket: TIdTcpClient);
begin
  try
    pvSocket.Disconnect;
  except
  end;
end;

end.
