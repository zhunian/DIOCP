unit uWSocketTools;

interface

uses
  Classes, OverbyteIcsWSocket, SysUtils, IdTCPClient;

type
  TWSocketTools = class(TObject)
  public
    class function recvBuffer(pvSocket:TWSocket; buf: Pointer; len: Cardinal):
        Integer;
    class function sendBuffer(pvSocket:TWSocket; buf: Pointer; len: Cardinal):
        Integer;
    class function sendStream(pvSocket:TWSocket; pvStream:TStream):Integer;
    
    class procedure slienceClose(pvSocket: TWSocket);
  end;

implementation

const
  BUF_BLOCK_SIZE = 1024 * 10;        // 10K 

class function TWSocketTools.recvBuffer(pvSocket: TWSocket;
    buf: Pointer; len: Cardinal): Integer;
begin
  Result := pvSocket.Receive(buf, len);
end;

class function TWSocketTools.sendBuffer(pvSocket: TWSocket;
  buf: Pointer; len: Cardinal): Integer;
begin
  Result := pvSocket.Send(buf, len);
end;

class function TWSocketTools.sendStream(pvSocket: TWSocket;
  pvStream: TStream): Integer;
var
  lvBufBytes:array[0..BUF_BLOCK_SIZE-1] of byte;
  l, j, lvTotal:Integer;
begin
  Result := -1;
  if pvStream = nil then Exit;
  if pvStream.Size = 0 then Exit;

  lvTotal :=0;

  pvStream.Position := 0;
  repeat
    l := pvStream.Read(lvBufBytes[0], SizeOf(lvBufBytes));
    if (l > 0) and (pvSocket.State <> wsClosed) then
    begin
      j:=sendBuffer(pvSocket, @lvBufBytes[0], l);
      if j <> l then
      begin
        raise Exception.CreateFmt('发送Buffer错误指定发送%d,实际发送:%d', [j, l]);
      end else
      begin
        lvTotal := lvTotal + j;
      end;
    end else Break;
  until (l = 0);
  Result := lvTotal;
end;

class procedure TWSocketTools.slienceClose(pvSocket: TWSocket);
begin
  try
    pvSocket.Close;
  except
  end;
end;

end.
