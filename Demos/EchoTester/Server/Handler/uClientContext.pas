unit uClientContext;
                               
interface

uses
  Windows, uBuffer, SyncObjs, Classes, SysUtils,
  uIOCPCentre, JSonStream;

type
  TClientContext = class(TIOCPClientContext)
  protected
    procedure DoConnect; override;
    procedure DoDisconnect; override;
    procedure DoOnWriteBack; override;
    procedure recvBuffer(buf:PAnsiChar; len:Cardinal); override;
  public





    

  end;

implementation






procedure TClientContext.DoConnect;
begin
  inherited;
end;

procedure TClientContext.DoDisconnect;
begin
  
  inherited;
end;



procedure TClientContext.DoOnWriteBack;
begin
  inherited;
end;

procedure TClientContext.recvBuffer(buf: PAnsiChar; len: Cardinal);
begin
  //直接投递回去
  postSendBuffer(buf, len);

  // 清理读取的
  clearRecvedBuffer;
end;

end.
